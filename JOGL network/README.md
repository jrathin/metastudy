# How to generate those files

To generate those files use the python and R code in the code folder.

For more examples of what you can do with the JOGL API go see the example files in the JOGL api folder at the root of this repository.
