---
title: "R Notebook"
output: html_notebook
---

The goal of this code is to project the heterogenous network into single layers where links are weighted by the number of shared neighbors. For example, users are linked by the number of skills, projects, etc they have in common.

```{r}
require(igraph)
```

# Load network 

```{r}
G <- read.graph('JOGL_network.graphml', format = 'graphml')
```

```{r}
G
```

```{r}
unique(V(G)$type)
```

```{r}
# inds <- which(V(G)$type %in% c('Challenge', 'Program','Interest'))
# G <- delete_vertices(G, inds)
```



Function to take the power of a sparse matrix
```{r}
  `%^^%` = function(x, k) {for (i in 1:(k - 1)) x <- x %*% x; x}
```

```{r}
types <- unique(V(G)$type)

Gproj <- list()

for (type in types){
  print(type)
  
  Gtmp <- as.undirected(G)
  inds <- which(V(Gtmp)$type == type)
  
  # this computes the number of common neighbors between two nodes 
  A <- as_adjacency_matrix(Gtmp)
  B <- A%^^%2
  
  C <- as.matrix(B[inds,inds])
  
  # C[C<10] <- 0
  
  Gproj[[type]] <- graph_from_adjacency_matrix(C, weighted=T)
  
}
```


```{r}
dir.create('network_projections', showWarnings = FALSE)

for (type in types){
  write.graph(Gproj[[type]], file = paste0('network_projections/network_',
                                           type,'.graphml'),
                                           format='graphml')
  
}
```

