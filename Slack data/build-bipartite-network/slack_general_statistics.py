# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 16:06:10 2020

@author: lyubo

This code extracts some general statistics about slack

###################################
Instructions:
    change folderpath to adapt to the location of the Slack extract 
    (eg "OpenCOVID19_Initiative_Slack_export_Mar_2_2020")

    the code needs psutil to be installed
   
   you will also  need ORCA for visualisations:
    https://github.com/plotly/orca
###################################

"""

import pandas as pd
import json
import os
import plotly.express as px
import numpy as np
import networkx as nx



#Get general information about slack¶
#There are 3 files in each slack extract. Let us just look at them.


print('Loading data')

# load data for wards with IMD score
folderpath = "../OpenCOVID19 Initiative Slack export Mar 2 2020 - Mar 27 2020/"
filepath = 'channels.json' #"C:/Users/tupikina/Documents/Datasets/Dataward/2010_Ward_IMD.json"


with open(folderpath + filepath) as f:
    slack = json.load(f)
#ward_2010_data


print('Converting to dataframe')

df_channels = pd.DataFrame(slack)
#print(df_channels.shape)
#df_channels.head(10)

# convert each timestamp to time in datetime
df_channels['created'] =  pd.to_datetime(df_channels['created'],unit='s')#datetime.utcfromtimestamp(df_channels['created'].values.astype(int)/1000000000)#df_channels['created'].dt.to_pydatetime()

# sort values 
df_channels = df_channels.sort_values(by='created')

print('Saving to csv')
df_channels.to_csv('df_channels.csv')

#df = px.data.gapminder().query("country=='Canada'")
fig = px.line(df_channels, x="created", y="name", title='channel statistics')
fig.show()

fig.write_image("channels_creation.png")
#fig.savefig("channels_creation.png", dpi=150)


print('Computing network (overlap between channel members')
# get who is part of which channel and how channels overlap

channels = df_channels['name'].values  # numpy array

n_chan = len(channels)
matrix = np.zeros((n_chan,n_chan))

for ind in range(0,n_chan):
    for knd in range(0,n_chan): # n_chan*1./2
        #print(df_channels.iloc[ind,6])
        #print(type(df_channels.iloc[ind,6]))
        people1 =df_channels.iloc[ind,6] # list of people in channel ind
        people2 = df_channels.iloc[knd,6] ## list of people in channel knd
        matrix[ind, knd] = len(list(set(people1) & set(people2))) # number of people in overlap(people1, people2)
        
#print(matrix)

np.fill_diagonal(matrix, 0)

print('Saving as graph')
G = nx.from_numpy_matrix(matrix)
mapping = dict(zip(G, channels))
G = nx.relabel_nodes(G, mapping)

nx.write_graphml(G, 'channels_network.graphml')

#np.savetxt("matrix_overlap_channel.txt",matrix)
