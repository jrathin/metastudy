#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import sys
import pandas as pd

table = pd.read_json('projects_jogl.json', encoding='utf8')  



users = []
for each in table['users']:
    users.append(','.join([ str(t['id']) for t in each ]))

challenges = []
for each in table['challenges']:
    challenges.append(','.join([ str(t['id']) for t in each ]))


table['Users_num'] = users
table['Challenges_num'] = challenges


table.to_csv('projects_jogl.tsv', sep='\t', index=False)
