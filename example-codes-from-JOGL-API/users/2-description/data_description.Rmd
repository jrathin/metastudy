---
title: "R Notebook"
output: html_notebook
---

Loading data
```{r}
t <- read.delim('../1-extract-data/users_jogl.tsv')
```

```{r}
dim(t)
```

```{r, fig.width=3, fig.asp=1}
tb <- table(t$category)
names(tb)[names(tb)==''] <- 'Unknown'
df <- data.frame('Category'=names(tb),
                'Number'=as.numeric(tb))

df$Category <- factor(df$Category,levels = df$Category[order(-df$Number)])


ggplot(data=df, aes(x=Category, y=Number)) +
  geom_bar(stat="identity", fill="steelblue") +
    geom_text(aes(label=Number), vjust=-0.3, size=3.5)+
  xlab('')+
  ylab('Number of users')+
  theme(text = element_text(size=16),
    axis.text.x = element_text(angle = 90, hjust = 1))
```


```{r}
ggsave('plotCategories.png')
```

```{r, fig.width=3, fig.asp=1}
cities <- trimws(tolower(t$city))
cities <- sapply(cities, function(x) paste0(toupper(substring(x, 1,1)), substring(x, 2)))

tb <- table(cities)

names(tb)[names(tb)==''] <- 'Unknown'
df <- data.frame('Category'=names(tb),
                'Number'=as.numeric(tb))

df$Category <- factor(df$Category,levels = df$Category[order(-df$Number)])

df <- df[-which(df$Category=='Unknown'),]


ggplot(data=df, aes(x=Category, y=Number)) +
  geom_bar(stat="identity", fill="steelblue") +
    geom_text(aes(label=Number), vjust=-0.3, size=2)+
  xlab('')+
  ylab('Number of users')+
  # scale_y_log10()+
  theme(text = element_text(size=8),
    axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.1))
```


```{r}
ggsave('plotCities.png')
```


## Skills to user

```{r}
```


```{r}
df_skills <- do.call(rbind, lapply(1:nrow(t), function(i){
  name <- paste(t$first_name[i], t$last_name[i])
  skills <- gsub('\\[|\\]|\'','',t$skills[i])
  skills <- trimws(tolower(strsplit(skills, split = ',')[[1]]))
  N <- length(skills)
  data.frame('Name'=rep(name, N), 'Skills'=skills)
}))
```

```{r}
require(igraph)

G <- graph_from_data_frame(df_skills)
```

```{r}
V(G)$type <- bipartite_mapping(G)$type
```

```{r}
Gproj <- bipartite_projection(G)
G_users <- Gproj$proj1
G_events <- Gproj$proj2

write.graph(G_users,'network_users_per_skills.graphml',format='graphml')
write.graph(G_events,'network_skills_per_users.graphml',format='graphml')
```


## some statistics

```{r, fig.asp=1,fig.width=3, warning=FALSE}
tb <- table(table(df_skills$Name))

x <- as.numeric(names(tb))
y <- as.numeric(tb)
y <- sum(y) - cumsum(y)
df <-data.frame(x,y)
# Basic scatter plot

ggplot(df, aes(x=x, y=y)) + 
  geom_point(size=2) +
  scale_y_log10() + 
  xlab('Number of skills') +
  ylab('Number of users with less than x skills') +
  theme_gray(base_size = 20) 

```
```{r}
ggsave('plotSkillsPerParticipants.pdf')
```

```{r, fig.asp=1,fig.width=3, warning=FALSE}
tb <- table(table(df_skills$Skills))

x <- as.numeric(names(tb))
y <- as.numeric(tb)
y <- sum(y) - cumsum(y)
df <-data.frame(x,y)
# Basic scatter plot

ggplot(df, aes(x=x, y=y)) + 
  geom_point(size=2) +
  scale_x_log10() + 
  scale_y_log10() + 
  xlab('Number of users') +
  ylab('Number of skills with less than x users') +
  theme_gray(base_size = 20)

```
```{r}
ggsave('plotParticipantsPerSkills.pdf')
```

# Most popular skills

```{r}
best_skills <- names(sort(table(df_skills$Skills), decreasing = TRUE)[1:20])
```

```{r, fig.width=3, fig.asp=1}
tb <- table(df_skills$Skills)
tb <- tb[match(best_skills, names(tb))]
df <- data.frame('Skills'=names(tb),
                'Number of users'=as.numeric(tb))

df$Skills <- factor(df$Skills,levels = df$Skills[order(-df$Number.of.users)])


ggplot(data=df, aes(x=Skills, y=Number.of.users)) +
  geom_bar(stat="identity", fill="steelblue") +
    geom_text(aes(label=Number.of.users), vjust=-0.3, size=3.5)+
  xlab('')+
  ylab('Number of users')+
  ggtitle('Top skills on JOGL')+
  theme(text = element_text(size=16),
    axis.text.x = element_text(angle = 90, hjust = 1))
```

```{r}
ggsave('plotTopSkillsOnJogl.png')
```